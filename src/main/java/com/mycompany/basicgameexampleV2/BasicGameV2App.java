package com.mycompany.basicgameexampleV2;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.core.serialization.Bundle;
import com.almasb.fxgl.entity.Entity;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.util.Map;
import javafx.scene.input.KeyCode;
import com.almasb.fxgl.input.Input;
import com.almasb.fxgl.multiplayer.MultiplayerService;
import com.almasb.fxgl.net.Connection;
import com.almasb.fxgl.physics.CollisionHandler;
import javafx.util.Duration;

import static com.almasb.fxgl.dsl.FXGL.*;

/**
 *
 * @author Carlton Davis
 */
/*Basic Game app example code with multiplayer functionality. Adopted from 
  Almas Baimagambetov sample FXGL games code located at 
  https://github.com/AlmasB/FXGLGames */
public class BasicGameV2App extends GameApplication {

    //Used to determine whether node is a server
    private boolean isServer = false;

    //Stores the connection handle for connection between client and server
    private Connection<Bundle> connection;

    //Class field for player; player1 on server and player2 on client
    private Entity player1;
    private Entity player2;

    //Class field for orange.
    private Entity orange;
    
    //Needed to handle input to the client.
    private Input clientInput;

    //Set up the game window
    @Override
    protected void initSettings(GameSettings settings) {
        settings.setWidth(700);
        settings.setHeight(700);
        settings.setTitle("Basic Game v2 App");
        settings.setVersion("0.2");
        settings.addEngineService(MultiplayerService.class); //Required for muliplayer servcie
    }

    //Setup input handling code
    @Override
    protected void initInput() {
        onKey(KeyCode.RIGHT, () -> {
            player1.translateX(5); // move right 5 pixels
            inc("pixelsMoved", +5);
            play("game-sound.wav");
        });

        onKey(KeyCode.LEFT, () -> {
            player1.translateX(-5); // move left 5 pixels
            inc("pixelsMoved", -5);
            play("game-sound.wav");
        });

        onKey(KeyCode.UP, () -> {
            player1.translateY(-5); // move up 5 pixels
            inc("pixelsMoved", +5);
            play("game-sound.wav");
        });

        onKey(KeyCode.DOWN, () -> {
            player1.translateY(5); // move down 5 pixels
            inc("pixelsMoved", +5);
            play("game-sound.wav");
        });

        //Used for setting up input on the client
        clientInput = new Input();

        onKeyBuilder(clientInput, KeyCode.RIGHT)
                .onAction(() -> {
                    player2.translateX(5);
                    inc("pixelsMoved", +5);
                    play("game-sound.wav");
                });

        onKeyBuilder(clientInput, KeyCode.LEFT)
                .onAction(() -> {
                    player2.translateX(-5);
                    inc("pixelsMoved", -5);
                    play("game-sound.wav");
                });

        onKeyBuilder(clientInput, KeyCode.UP)
                .onAction(() -> {
                    player2.translateY(-5);
                    inc("pixelsMoved", +5);
                    play("game-sound.wav");
                });

        onKeyBuilder(clientInput, KeyCode.DOWN)
                .onAction(() -> {
                    player2.translateY(5);
                    inc("pixelsMoved", +5);
                    play("game-sound.wav");
                });
    }

    //Create Game play variable
    @Override
    protected void initGameVars(Map<String, Object> vars) {
        vars.put("pixelsMoved", 0);
    }

    //This method contains code needed to start the game.
    @Override
    protected void initGame() {

        runOnce(() -> {
            getDialogService().showConfirmationBox("Are you the host?", yes -> {
                isServer = yes;

                //Add background color to the game window.
                getGameScene().setBackgroundColor(Color.rgb(153, 204, 255));
                
                //this line is needed in order for entities to be spawned
                getGameWorld().addEntityFactory(new BasicGameFactory());

                if (isServer) {
                    //Setup the TCP port that the server will listen at.
                    var server = getNetService().newTCPServer(7777);
                    server.setOnConnected(conn -> {
                        connection = conn;
                        
                        //Setup the entities and other necessary items on the server.
                        getExecutor().startAsyncFX(() -> onServer());
                    });
                    
                    //Start listening on the specified TCP port.
                    server.startAsync();
                    
                } else {
                    //Setup the connection to the server.
                    var client = getNetService().newTCPClient("localhost", 7777);
                    client.setOnConnected(conn -> {
                        connection = conn;
                        
                        //Enable the client to receive data from the server.
                        getExecutor().startAsyncFX(() -> onClient());
                    });
                    
                    //Establish the connection to the server.
                    client.connectAsync();
                }
            });
        }, Duration.seconds(0.5));
    }

    //Code to setup entities on other necessities on the server.
    private void onServer() {

        //Spawn the player for the server
        player1 = spawn("player", 300, 300);
        getService(MultiplayerService.class).spawn(connection, player1, "player");

        player2 = spawn("player", 100, 100);
        getService(MultiplayerService.class).spawn(connection, player2, "player");

        orange = spawn("orange", 500, 200);
        getService(MultiplayerService.class).spawn(connection, orange, "orange");

        getService(MultiplayerService.class).addInputReplicationReceiver(connection, clientInput);

        initPhysics();
    }

    //Code to setup the client
    private void onClient() {
        //Initializes player1; segmentation fault occurs if this line is missing.
        player1 = new Entity();

        getService(MultiplayerService.class).addEntityReplicationReceiver(connection, getGameWorld());
        getService(MultiplayerService.class).addInputReplicationSender(connection, getInput());
    }

    //Code to run when the entities collide
    @Override
    protected void initPhysics() {
        getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.ORANGE) {
            //Order of types is the same as passed into the constructor
            @Override
            protected void onCollisionBegin(Entity player, Entity orange) {
                play("game-sound.wav");
                orange.removeFromWorld();
            }
        });
    }

    //Setup UI for the game
    @Override
    protected void initUI() {
        Text textPixels = new Text();
        textPixels.setTranslateX(50); // x = 50
        textPixels.setTranslateY(100); // y = 100

        /*Bind the UI text object to the variable pixelsMoved. This allows
          the UI text to automatically show the number of pixels the player
          has moved.
         */
        textPixels.textProperty().bind(getWorldProperties().intProperty("pixelsMoved").asString());

        //Add the UI node to the scene graph
        getGameScene().addUINode(textPixels);

    }

    //This method is needed in order to move the client player.
    @Override
    protected void onUpdate(double tpf) {
        if (isServer) {
            clientInput.update(tpf);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
