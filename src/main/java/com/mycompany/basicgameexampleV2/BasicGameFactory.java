package com.mycompany.basicgameexampleV2;

import static com.almasb.fxgl.dsl.FXGL.entityBuilder;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import com.almasb.fxgl.entity.components.CollidableComponent;
import com.almasb.fxgl.multiplayer.NetworkComponent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Carlton Davis
 */
//Entity Factory to spawn the entities for the game
public class BasicGameFactory implements EntityFactory {

    @Spawns("player")
    public Entity newPlayer1(SpawnData data) {
        return entityBuilder(data)
                .type(EntityType.PLAYER)
                .viewWithBBox("bird-sprite.jpg") //Generate the entity bounding box.
                .with(new CollidableComponent(true))
                .with(new NetworkComponent()) //Needed for network service
                .build();
    }

    @Spawns("orange")
    public Entity newOrange1(SpawnData data) {
        return entityBuilder(data)
                .type(EntityType.ORANGE)
                .viewWithBBox(new Circle(10, 10, 10, Color.ORANGE)) //Generate the entity bounding box.
                .with(new CollidableComponent(true))
                .with(new NetworkComponent()) //Needed for network service
                .build();
    }

}
